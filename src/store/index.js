import Vue from 'vue'
import Vuex from 'vuex'

import meetup from './meetup'
import shared from './shared'
import user from './user'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    meetup,
    shared,
    user
  }
})
