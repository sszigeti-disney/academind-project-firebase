import * as firebase from 'firebase'

const firebaseRefUsersName = 'users'
const firebaseRefRegistrationsName = 'registrations'

/* TODO: a common pattern here is this:
- clearError
- setLoading true
- wait for promise
  - process success
  - handle error
    - setError
- setLoading false

I should factor this out to a helper function that accepts the "promise" and the "process success" as callbacks (and perhaps the error handling as an optional callback)
*/

export default {
  actions: {
    autoSignIn ({ commit }, payload) {
      commit('setUser', {
        id: payload.uid,
        fbKeys: {},
        registeredMeetups: []
      })
    },

    fetchUserData ({ commit, getters }) {
      commit('clearError')
      commit('setLoading', true)
      firebase.database().ref(`${firebaseRefUsersName}/${getters.user.id}/${firebaseRefRegistrationsName}`).once('value').then(registrationsData => {
        const registeredMeetupsMap = registrationsData.val()
        let registeredMeetups = []
        let fbKeys = {}
        if (registeredMeetupsMap) {
          registeredMeetups = Object.values(registeredMeetupsMap)
          fbKeys = (registeredMeetupsMap => {
            const fbKeys = {}
            for (const key in registeredMeetupsMap) {
              fbKeys[registeredMeetupsMap[key]] = key
            }
            return fbKeys
          })(registeredMeetupsMap)
        }
        const updatedUser = {
          fbKeys,
          id: getters.user.id,
          registeredMeetups
        }
        commit('setUser', updatedUser)
        commit('setLoading', false)
      }).catch(err => {
        console.log('fetchUserData error handling')
        commit('setLoading', false)
        commit('setError', err)
      })
    },

    logOut ({ commit }, next) {
      commit('clearError')
      commit('setLoading', true)
      firebase.auth().signOut().then(() => {
        commit('setUser', null)
        commit('setLoading', false)
        next()
      }).catch(err => {
        commit('setLoading', false)
        commit('setError', err)
        next()
      })
    },

    registerUserForMeetup ({ commit, getters }, meetupId) {
      commit('clearError')
      commit('setLoading', true)
      firebase.database().ref(`${firebaseRefUsersName}/${getters.user.id}/${firebaseRefRegistrationsName}`).push(meetupId).then(registrationData => {
        commit('registerUserForMeetup', {
          fbKey: registrationData.key,
          id: meetupId
        })
        commit('setLoading', false)
      }).catch(err => {
        commit('setLoading', false)
        commit('setError', err)
      })
    },

    signUserUp ({ commit }, payload) {
      commit('clearError')
      commit('setLoading', true)
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password).then(user => {
        commit('setLoading', false)
        const newUser = {
          id: user.uid,
          fbKeys: {},
          registeredMeetups: []
        }
        commit('setUser', newUser)
      }).catch(err => {
        commit('setLoading', false)
        commit('setError', err)
      })
    },

    signUserIn ({ commit }, payload) {
      commit('clearError')
      commit('setLoading', true)
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password).then(user => {
        const newUser = {
          id: user.uid,
          registeredMeetups: [] // TODO: actually retrieve data
        }
        commit('setUser', newUser)
        commit('setLoading', false)
      }).catch(err => {
        commit('setLoading', false)
        commit('setError', err)
      })
    },

    unregisterUserFromMeetup ({ commit, getters }, meetupId) {
      commit('clearError')
      commit('setLoading', true)
      const fbKey = getters.user.fbKeys[meetupId]
      firebase.database().ref(`${firebaseRefUsersName}/${getters.user.id}/${firebaseRefRegistrationsName}`).child(fbKey).remove().then(() => {
        commit('unregisterUserFromMeetup', meetupId)
        commit('setLoading', false)
      }).catch(err => {
        commit('setLoading', false)
        commit('setError', err)
      })
    }
  },

  getters: {
    user (state) {
      return state.user
    }
  },

  mutations: {
    registerUserForMeetup (state, payload) {
      const meetupId = payload.id
      if (state.user.registeredMeetups.findIndex(meetup => meetup.id === meetupId) > -1) {
        return
      }
      state.user.registeredMeetups.push(meetupId)
      state.user.fbKeys[meetupId] = payload.fbKey
    },

    setUser (state, payload) {
      state.user = payload
    },

    unregisterUserFromMeetup (state, payload) {
      const registeredMeetups = state.user.registeredMeetups
      registeredMeetups.splice(registeredMeetups.findIndex(meetup => meetup.id === payload.id), 1)
      Reflect.deleteProperty(state.user.fbKeys, payload.id)
    }
  },

  state: {
    user: null
  }
}
