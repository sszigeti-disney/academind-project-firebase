export default {
  getters: {
    error (state) {
      return state.error
    },

    loading (state) {
      return state.loading
    }
  },

  mutations: {
    clearError (state) {
      state.error = null
    },

    setError (state, payload) {
      state.error = payload
    },

    setLoading (state, payload) {
      state.loading = payload
    }
  },

  state: {
    error: null,
    loading: false
  }
}
