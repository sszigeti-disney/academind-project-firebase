import * as firebase from 'firebase'

const firebaseRefMeetupsName = 'meetups'

/* TODO: a common pattern here is this:
- clearError
- setLoading true
- wait for promise
  - process success
  - handle error
    - setError
- setLoading false

I should factor this out to a helper function that accepts the "promise" and the "process success" as callbacks (and perhaps the error handling as an optional callback)
*/

export default {
  actions: {
    createMeetup ({ commit, getters }, payload) {
      commit('clearError')
      commit('setLoading', true)
      const meetup = {
        creatorId: getters.user.id,
        date: payload.date.toISOString(),
        description: payload.description,
        location: payload.location,
        title: payload.title
      }
      let imageUrl = null
      let meetupId = null
      firebase.database().ref(firebaseRefMeetupsName).push(meetup).then(firebaseResponseData => {
        meetupId = firebaseResponseData.key
      }).then(() => {
        const filename = payload.image.name
        const extension = filename.slice(filename.lastIndexOf('.'))
        const imageStorageName = `${firebaseRefMeetupsName}/${meetupId}.${extension}`
        return firebase.storage().ref(imageStorageName).put(payload.image)
      }).then(fileData => {
        imageUrl = fileData.metadata.downloadURLs[0]
        return firebase.database().ref(firebaseRefMeetupsName).child(meetupId).update({ imageUrl })
      }).then(() => {
        commit('createMeetup', {
          ...meetup,
          id: meetupId,
          imageUrl
        })
        commit('setLoading', false)
      }).catch(err => {
        commit('setLoading', false)
        commit('setError', err)
      })
    },

    loadMeetups ({ commit }) {
      commit('clearError')
      commit('setLoading', true)
      firebase.database().ref(firebaseRefMeetupsName).once('value').then(firebaseResponseData => {
        const meetups = []
        const firebaseValues = firebaseResponseData.val()
        if (firebaseValues) {
          Object.keys(firebaseValues).forEach(id => {
            meetups.push({
              ...firebaseValues[id],
              id
            })
          })
        }
        commit('setLoadedMeetups', meetups)
        commit('setLoading', false)
      }).catch(err => {
        commit('setLoading', false)
        commit('setError', err)
      })
    },

    updateMeetup ({ commit }, payload) {
      commit('clearError')
      commit('setLoading', true)
      const updatePayload = {}
      if (payload.title) {
        updatePayload.title = payload.title
      }
      if (payload.description) {
        updatePayload.description = payload.description
      }
      if (payload.date) {
        updatePayload.date = payload.date
      }
      firebase.database().ref(firebaseRefMeetupsName).child(payload.id).update(updatePayload).then(() => {
        commit('updateMeetup', payload)
        commit('setLoading', false)
      }).catch(err => {
        commit('setLoading', false)
        commit('setError', err)
      })
    }
  },

  getters: {
    featuredMeetups (state, getters) {
      return getters.loadedMeetups.slice(0, 5)
    },

    loadedMeetup (state) {
      return meetupId => state.loadedMeetups.find(meetup => meetup.id === meetupId)
    },

    loadedMeetups (state) {
      return state.loadedMeetups.sort((meetupA, meetupB) => meetupA.date > meetupB.date)
    }
  },

  mutations: {
    createMeetup (state, payload) {
      state.loadedMeetups.push(payload)
    },

    setLoadedMeetups (state, payload) {
      state.loadedMeetups = payload
    },

    updateMeetup (state, payload) {
      const meetup = state.loadedMeetups.find(meetup => meetup.id === payload.id)
      if (payload.title) {
        meetup.title = payload.title
      }
      if (payload.description) {
        meetup.description = payload.description
      }
      if (payload.date) {
        meetup.date = payload.date
      }
    }
  },

  state: {
    loadedMeetups: []
  }
}
