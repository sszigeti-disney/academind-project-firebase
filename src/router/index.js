import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/home'
import Meetups from '@/components/meetup/meetups'
import Meetup from '@/components/meetup/meetup'
import CreateMeetup from '@/components/meetup/create-meetup'
import Profile from '@/components/user/profile'
import SignUp from '@/components/user/sign-up'
import SignIn from '@/components/user/sign-in'
import SignOut from '@/components/user/sign-out'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/meetups',
      name: 'meetups',
      component: Meetups
    },
    {
      path: '/meetup/new',
      name: 'create-meetup',
      component: CreateMeetup,
      beforeEnter: AuthGuard
    },
    {
      path: '/meetup/:id',
      name: 'meetup',
      component: Meetup,
      props: true
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      component: SignUp
    },
    {
      path: '/sign-in',
      name: 'sign-in',
      component: SignIn
    },
    {
      path: '/sign-out',
      name: 'sign-out',
      component: SignOut,
      beforeEnter: AuthGuard
    }
  ],
  mode: 'history'
})
