// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'
import { store } from './store'
import DateFilter from './filters/date'
import * as firebase from 'firebase'
import Alert from './components/_shared/alert'

Vue.use(Vuetify, {
  theme: {
    primary: colors.red.darken2,
    accent: colors.red.accent2,
    secondary: colors.grey.lighten1,
    info: colors.blue.lighten1,
    warning: colors.amber.darken2,
    error: colors.red.accent4,
    success: colors.green.lighten2
  }
})

Vue.config.productionTip = false

Vue.filter('date', DateFilter)

Vue.component('app-alert', Alert)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store,
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyCa2-H8w_lsB21ELyO_ll9hVDT1KjJKfIg',
      authDomain: 'learningvuetify.firebaseapp.com',
      databaseURL: 'https://learningvuetify.firebaseio.com',
      projectId: 'learningvuetify',
      storageBucket: 'learningvuetify.appspot.com'
    })

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
        this.$store.dispatch('fetchUserData') // FIXME: since autoSignIn is async, I think fetchUserData may happen too soon
      }
    })

    this.$store.dispatch('loadMeetups')
  }
})
